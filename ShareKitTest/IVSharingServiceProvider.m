//
//  IVSharingServiceProvider.m
//  ShareKitTest
//
//  Created by Ivan Vučica on 7.8.2012..
//  Copyright (c) 2012. Ivan Vučica. All rights reserved.
//

#import "IVSharingServiceProvider.h"
#import "IVSharingWindowController.h"

@implementation IVSharingServiceProvider
+ (void)load
{
  FILE * f= fopen("/tmp/ivsharingserviceprovider", "w");
    printf("Loaded\n");
  if(f) fclose(f);
}

+ (id)servicesForItemTypes:(NSSet*)itemTypes;
{
    NSImage * image = [[[NSImage alloc] initWithContentsOfFile:[[NSBundle bundleForClass:self.class] pathForImageResource:@"image.png"]] autorelease];
    NSLog(@"%@", NSStringFromSelector(_cmd));
    SHKSharingService * service = [SHKSharingService sharingServiceWithTitle:@"The Sharing Service 1" image:image alternateImage:image handler:nil];
    SHKSharingService * service2 = [SHKSharingService sharingServiceWithIdentifier:@"post" provider:[self class] title:@"The Sharing Service 2" image:image alternateImage:image];
    
    service2.shouldDimSourceWindow = YES;
    return [NSArray arrayWithObjects:service, service2, nil];
}
+ (id)serviceWithIdentifier:(NSString*)identifier;
{
    NSLog(@"%@", NSStringFromSelector(_cmd));
    if([identifier isEqualToString:@"post"])
    {
        NSImage * image = [[[NSImage alloc] initWithContentsOfFile:[[NSBundle bundleForClass:self.class] pathForImageResource:@"image.png"]] autorelease];

        SHKSharingService * service2 = [SHKSharingService sharingServiceWithIdentifier:@"post" provider:[self class] title:@"The Sharing Service 2" image:image alternateImage:image];
        service2.shouldDimSourceWindow = YES;
        return service2;
    }
    return nil;
}
+ (Class)windowControllerClass;
{
    NSLog(@"%@ %@", [self class], NSStringFromSelector(_cmd));
    return [IVSharingWindowController class];
}
+ (id)serviceImageNamed:(id)arg1
{
    
    NSLog(@"%@ %@ - %@", [self class], NSStringFromSelector(_cmd), arg1);
    return [super serviceImageNamed:arg1];
}
- (void)serviceWindowWasValidated;
{
    NSLog(@"%@ %@", [self class], NSStringFromSelector(_cmd));
    [super serviceWindowWasValidated];
    [self.windowController closeWindowWithSuccess:YES];
}
- (void)prepareUserConfigurationUI;
{
    NSLog(@"%@ %@", [self class], NSStringFromSelector(_cmd));
    [super prepareUserConfigurationUI];
}
- (void)prepareUI;
{
    NSLog(@"%@ %@", [self class], NSStringFromSelector(_cmd));
    [super prepareUI];
    if(!self.windowController.window)
        [self.windowController loadWindow];
}
- (void)userRequestedConfigurationAction;
{
    NSLog(@"%@ %@", [self class], NSStringFromSelector(_cmd));
    [super userRequestedConfigurationAction];
}
- (BOOL)serviceNeedsUserConfiguration;
{
    
    NSLog(@"DESC: %@", self.description);
    
    NSLog(@"%@ %@", [self class], NSStringFromSelector(_cmd));
#if 1 
    // in case we have no support for configuring account
    // at the system level, we want to just return "NO".
    return NO;
#else
    return [super serviceNeedsUserConfiguration];
#endif
}
- (id)noConfiguredAccountImage
{
    NSLog(@"%@ %@", [self class], NSStringFromSelector(_cmd));
    //NSLog(@" (%@)", [super noConfiguredAccountImage]);
    
    NSImage * image = [[[NSImage alloc] initWithContentsOfFile:[[NSBundle bundleForClass:self.class] pathForImageResource:@"setup-logo-ivssp"]] autorelease];
    return image;
    
    // default: returns setup-logo-long.png.
    // why the suffix "long"?
    //return [super noConfiguredAccountImage];
}

- (void)dealloc;
{
  [super dealloc];
}
- (id)window
{
    
    NSLog(@"%@ %@", [self class], NSStringFromSelector(_cmd));
    NSLog(@" (%@)", [super window]);
    return [super window];
}

#pragma mark - Used with SHKShareWindowController 

- (long long)remainingCharactersCount
{
    return 50;
}
/*
- (BOOL)validateActionButton
{
    return YES;
}
- (void)textContentDidChange
{
}
 */
+(void) encodeWithCoder:(NSCoder*)coder
{
    // called for some reason.
}
#pragma mark - Used with shkmicrobloggingserviceprovider

+ (id)accountTypeIdentifier;
{
    NSLog(@"%@", NSStringFromSelector(_cmd));
    return @"ivssp";
}

@end
