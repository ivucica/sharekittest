//
//  SHKServicesManager+DisableSignatureCheck.m
//  ShareKitTest
//
//  Created by Ivan Vučica on 11.8.2012..
//  Copyright (c) 2012. Ivan Vučica. All rights reserved.
//

#import "SHKServicesManager+DisableSignatureCheck.h"

@implementation SHKServicesManager (DisableSignatureCheck)

+ (void)load
{
    NSLog(@"DisableSignatureCheck");
}

- (BOOL)isFileSignedByAppleAtURL:(id)arg1
{
    NSLog(@"%@%@", NSStringFromSelector(_cmd), arg1);
    return YES;
}

@end
