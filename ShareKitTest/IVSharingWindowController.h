//
//  IVSharingWindowController.h
//  ShareKitTest
//
//  Created by Ivan Vučica on 7.8.2012..
//  Copyright (c) 2012. Ivan Vučica. All rights reserved.
//

#import "ShareKit.h"

@interface IVSharingWindowController : SHKShareWindowController //SHKWindowController
- (IBAction)send:(id)sender;
- (IBAction)cancel:(id)sender;
@end
