//
//  IVSharingServiceProvider.h
//  ShareKitTest
//
//  Created by Ivan Vučica on 7.8.2012..
//  Copyright (c) 2012. Ivan Vučica. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "ShareKit.h"
@interface IVSharingServiceProvider :
#if 0
SHKMicrobloggingServiceProvider
#else
SHKShareWindowServiceProvider
//SHKWindowServiceProvider
#endif

+ (id)accountTypeIdentifier;
+ (id)servicesForItemTypes:(id)arg1;
+ (id)serviceWithIdentifier:(id)arg1;
+ (Class)windowControllerClass;
- (void)startCompletingText:(id)arg1 inRange:(struct _NSRange)arg2;
- (void)sendStatus:(id)arg1 images:(id)arg2 urls:(id)arg3 completionHandler:(id)arg4;
- (void)tearDownSession;
- (void)updateSession;
- (void)textContentDidChange;
- (void)serviceWindowWasValidated;
- (void)prepareUserConfigurationUI;
- (void)prepareUI;
- (void)userRequestedConfigurationAction;
- (BOOL)serviceNeedsUserConfiguration;
- (id)session;
- (void)renewSession;
- (void)dealloc;


@end
