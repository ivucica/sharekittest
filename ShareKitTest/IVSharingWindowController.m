//
//  IVSharingWindowController.m
//  ShareKitTest
//
//  Created by Ivan Vučica on 7.8.2012..
//  Copyright (c) 2012. Ivan Vučica. All rights reserved.
//

#import "IVSharingWindowController.h"

@interface IVSharingWindowController ()

@end

@implementation IVSharingWindowController

- (id)init
{
    self = [super init];
    if(!self)
        return nil;
    
    return self;
}
- (id)initWithWindow:(NSWindow *)window
{
    self = [super initWithWindow:window];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}
- (NSString *)windowNibPath
{
    return [[NSBundle bundleForClass:[self class]] pathForResource:@"IVSharingWindowController" ofType:@"nib"];
}
- (void)loadWindow
{
    [super loadWindow];
    if(!self.window)
    {
        NSNib * nib = [[[NSNib alloc] initWithContentsOfURL:[NSURL fileURLWithPath:self.windowNibPath]] autorelease];
        NSArray * array = [NSArray array];
        [nib instantiateWithOwner:self topLevelObjects:&array];
    }
}

+ (id)keyPathsForValuesAffectingImage;
{
    NSLog(@"%@ %@", [self class], NSStringFromSelector(_cmd));
    return nil;
}

- (struct CGRect)imageContentFrameForItem:(id)arg1;
{
    NSLog(@"%@ %@", [self class], NSStringFromSelector(_cmd));
    return [super imageContentFrameForItem:arg1];
}
- (struct CGRect)imageFrameForItem:(id)arg1;
{
    NSLog(@"%@ %@", [self class], NSStringFromSelector(_cmd));
    return [super imageFrameForItem:arg1];
}
- (id)imageForItem:(id)arg1;
{
    NSLog(@"%@ %@", [self class], NSStringFromSelector(_cmd));
    return [super imageForItem:arg1];
}
- (id)mainItem;
{
    NSLog(@"%@ %@", [self class], NSStringFromSelector(_cmd));
    return [super mainItem];
}

- (void)setImage:(id)arg1 decorationHint:(int)arg2 contentRect:(struct CGRect)arg3;
{
    NSLog(@"%@ %@", [self class], NSStringFromSelector(_cmd));
    [super setImage:arg1 decorationHint:arg2 contentRect:arg3];
}
- (void)updateWindowFrameToAdjustTextView:(id)arg1;
{
    NSLog(@"%@ %@", [self class], NSStringFromSelector(_cmd));
    [super updateWindowFrameToAdjustTextView:arg1];
}
- (double)_textHeightForTextView:(id)arg1;
{
    NSLog(@"%@ %@", [self class], NSStringFromSelector(_cmd));
    return [super _textHeightForTextView:arg1];
}
- (void)closeWindowWithSuccess:(BOOL)arg1;
{
    NSLog(@"%@ %@", [self class], NSStringFromSelector(_cmd));
    [super closeWindowWithSuccess:arg1];
}
- (id)effectWithItems:(id)arg1;
{
    NSLog(@"%@ %@", [self class], NSStringFromSelector(_cmd));
    return [super effectWithItems:arg1];
}
- (void)setWindowFrame:(struct CGRect)arg1 level:(unsigned long long)arg2;
{
    NSLog(@"%@ %@", [self class], NSStringFromSelector(_cmd));
    [super setWindowFrame:arg1 level:arg2];
}
- (struct CGRect)windowFrame;
{
    NSLog(@"%@ %@", [self class], NSStringFromSelector(_cmd));
    return [super windowFrame];
}
- (void)showWindowForItems:(id)arg1;
{
    NSLog(@"%@ %@", [self class], NSStringFromSelector(_cmd));
    [super showWindowForItems:arg1];
}
- (void)windowDidLoad;
{
    NSLog(@"%@ %@", [self class], NSStringFromSelector(_cmd));
    [super windowDidLoad];
}
- (void)windowDidClose;
{
    NSLog(@"%@ %@", [self class], NSStringFromSelector(_cmd));
    [super windowDidClose];
}
- (void)windowWillClose;
{
    NSLog(@"%@ %@", [self class], NSStringFromSelector(_cmd));
    [super windowWillClose];
}
- (void)windowDidShow;
{
    NSLog(@"%@ %@", [self class], NSStringFromSelector(_cmd));
    [super windowDidShow];
    //[self setWindowFrame:self.window.frame level:NSWindowAbove];
}
- (void)windowWillShow;
{
    NSLog(@"%@ %@", [self class], NSStringFromSelector(_cmd));
    [super windowWillShow];
}

- (IBAction)ok:(id)sender
{
    [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:@"https://plus.google.com/share?url=Hello"]];
    [self closeWindowWithSuccess:YES];
}
/*
- (IBAction)cancel:(id)sender
{
    [self closeWindowWithSuccess:NO];
}
 */
@end
